<?php
/**
 * The base configuration for WordPress
 *
 * The wp-config.php creation script uses this file during the
 * installation. You don't have to use the web site, you can
 * copy this file to "wp-config.php" and fill in the values.
 *
 * This file contains the following configurations:
 *
 * * MySQL settings
 * * Secret keys
 * * Database table prefix
 * * ABSPATH
 *
 * @link https://codex.wordpress.org/Editing_wp-config.php
 *
 * @package WordPress
 */

// ** MySQL settings - You can get this info from your web host ** //
/** The name of the database for WordPress */
define('DB_NAME', 'wp');

/** MySQL database username */
define('DB_USER', 'root');

/** MySQL database password */
define('DB_PASSWORD', 'monika');

/** MySQL hostname */
define('DB_HOST', 'localhost');

/** Database Charset to use in creating database tables. */
define('DB_CHARSET', 'utf8');

/** The Database Collate type. Don't change this if in doubt. */
define('DB_COLLATE', '');

/**#@+
 * Authentication Unique Keys and Salts.
 *
 * Change these to different unique phrases!
 * You can generate these using the {@link https://api.wordpress.org/secret-key/1.1/salt/ WordPress.org secret-key service}
 * You can change these at any point in time to invalidate all existing cookies. This will force all users to have to log in again.
 *
 * @since 2.6.0
 */
define('AUTH_KEY',         'o,!%iK&:dw*[eQ-C5ti~FnA1G6%+%+CL58rx3w-GOld4ab+^~CGt-#jRv@VY_H#S');
define('SECURE_AUTH_KEY',  ';_#~lh5JEFE0^4_ZNn:MP*]TxoujOkY,-t-OS5([$(>)?E_%#Uh(rg<YO<.S3TP7');
define('LOGGED_IN_KEY',    '^9do<vi0l:~LHp[%W3-d2aJ?{.0I@C7sW+6{IE1Odmd_GFVP#}Y%IY6w1%@44{8V');
define('NONCE_KEY',        '5Nl`Q>NhQ?+Uf4TELn9a+3dE}Y-ntms6A#.Q?/,T#h(2R38wB[)}z^(qh-?6zdi8');
define('AUTH_SALT',        'U>&naiY{5zK]*_h#abhb{iCEa7[t1A`|5zu(Ajl(KR~/I95Q&Z.sz:R@~1a$h+%z');
define('SECURE_AUTH_SALT', 'K~6fK9G8CS{nx>||1:M/UcbdE9a3n}-FYT~];v9*${wr[j*c.!eS3TUI6^i3Hz{c');
define('LOGGED_IN_SALT',   '()opf^,56w4Y/g:Y7sc-TH98M1I.)i3/g{5t-d@8tRuJ4j~`KsoJT+8jzfWiXNvm');
define('NONCE_SALT',       'u5fJ_+x5dBTLvJ&c()#j=X[53sHXd#$u#Zh@6%y 2<5HEn5+$|s&sRVQ0S<:7x<c');

/**#@-*/

/**
 * WordPress Database Table prefix.
 *
 * You can have multiple installations in one database if you give each
 * a unique prefix. Only numbers, letters, and underscores please!
 */
$table_prefix  = 'wp_';

/**
 * For developers: WordPress debugging mode.
 *
 * Change this to true to enable the display of notices during development.
 * It is strongly recommended that plugin and theme developers use WP_DEBUG
 * in their development environments.
 *
 * For information on other constants that can be used for debugging,
 * visit the Codex.
 *
 * @link https://codex.wordpress.org/Debugging_in_WordPress
 */
define('WP_DEBUG', false);

/* That's all, stop editing! Happy blogging. */

/** Absolute path to the WordPress directory. */
if ( !defined('ABSPATH') )
	define('ABSPATH', dirname(__FILE__) . '/');

/** Sets up WordPress vars and included files. */
require_once(ABSPATH . 'wp-settings.php');
